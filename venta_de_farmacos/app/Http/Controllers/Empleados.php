<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmpleadoM;
use Illuminate\Support\Facades\DB;
use Datatables;

class Empleados extends Controller
{
	public function index(){

		$c=EmpleadoM::all();
		$title = "Empleados || Farmaceutica";
		echo view('templates/header',compact('title'));
		echo view('templates/navbar');
		echo view('EmpleadoV',compact('c'));
		echo view('templates/footer');
	}

	  public function empleadoList()
    {
        $users = DB::table('empleados')->select('*');
        return datatables()->of($empleados)
            ->make(true);
    }
}