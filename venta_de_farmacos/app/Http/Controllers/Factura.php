<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacturaM;
use Illuminate\Support\Facades\DB;
class Factura extends Controller
{
	public function index()
	{

		$c=FacturaM::all();
		$title = "Factura || Farmaceutica";
		echo view('templates/header',compact('title'));
		echo view('templates/navbar');
		echo view('FacturaV',compact('c'));
		echo view('templates/footer');
	}
}

