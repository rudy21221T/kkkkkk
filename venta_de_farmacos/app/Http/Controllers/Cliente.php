<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClienteM;
use Illuminate\Support\Facades\DB;
class Cliente extends Controller
{  
	public $ClienteM;
	public function __construct(ClienteM $ClienteM){
		$this->ClienteM = $ClienteM;
	}

	public function index()
	{

		$c = $this->ClienteM->getCliente();
		$title = "Cliente || Farmaceutica";
		echo view('templates/header',compact('title'));
		echo view('templates/navbar');
		echo view('ClienteV',compact('c'));
		echo view('templates/footer');
	}


    //parametros recibidos: id
    //parametros enviados: data,title
	public function Pedido($id){
		$data['c']= DB::table('clientes as c')->join('ciudad as ci','ci.id_ciudad','=','c.id_ciudad')->where('id_cliente', '=', $id)->get();
		$data['p']= DB::table('productos as p')->join('proveedores as pro','pro.id_proveedor','=','p.id_proveedor')->join('ciudad as ci','ci.id_ciudad','=','pro.id_ciudad')->join('categoria as c','c.id_categoria','=','p.id_categoria')->get();
		$title = "Pedido || Farmaceutica";
		echo view('templates/header',compact('title'));
		echo view('templates/navbar');
		echo view('PedidoV',$data);
		echo view('templates/footer');
	}


	public function getPrecio(Request $request)
	{
		$id = request()->all(); 

		$respuesta = DB::table('productos as p')->join('proveedores as pro','pro.id_proveedor','=','p.id_proveedor')->join('ciudad as ci','ci.id_ciudad','=','pro.id_ciudad')->join('categoria as c','c.id_categoria','=','p.id_categoria')->where('p.id_producto',$id)->get();
		echo json_encode($respuesta);
	}

	public function eliminar($id){
		//accedemos a la base y su tabla indicamos con un where la busqueda y luego llamamos al metodo de eliminar.
		DB::table('clientes')->where('id_cliente', '=', $id)->delete();
		return redirect()->Route('index');
	}

	public function AgreagarProducto(Request $request)
	{
	

		
		
	}
}
