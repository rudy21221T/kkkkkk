<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ClienteM extends Model
{
	public $timestamps = false;
	protected $table = 'clientes';
	
	public function getCliente(){
		return DB::table('clientes as c')->join('ciudad as ci','ci.id_ciudad','=','c.id_ciudad')->get();
	}

}
