<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpleadoM extends Model
{
    public $timestamps = false;
	protected $table = 'empleados';
	protected $fillable=['id_empleado','nombre','apellido','dui','fecha_nacimiento','fecha_contratacion','direccion','telefono','correo','id_sexo'];
}
