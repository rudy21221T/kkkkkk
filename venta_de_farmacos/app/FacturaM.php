<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaM extends Model
{
    public $timestamps = false;
	protected $table = 'factura';
	protected $fillable=['id_factura','id_cliente','id_empleado','fecha_pedido'];
}
